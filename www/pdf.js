
/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
*/

(function () {
    var pdfPlugin = {};
    // special patch to correctly work on Ripple emulator (CB-9760)
    if (window.parent && !!window.parent.ripple) { // https://gist.github.com/triceam/4658021
        module.exports = window.open.bind(window); // fallback to default window.open behaviour
        return;
    }

    var exec = require('cordova/exec');
    var urlutil = require('cordova/urlutil');

    pdfPlugin.show = function (successBack,errorBack,file) {
        // Don't catch calls that write to existing frames (e.g. named iframes).

        cordova.exec(function(data){
            if(successBack){
                successBack(data.pageNum || "1")
            }
          },function(error){
              if(errorBack){
                  errorBack(error)
              }
          },"PDFViewer","showPDF",[{name:file.name || "",fileUrl:file.url,pageNum:file.num || "1"}])
    };

    module.exports = pdfPlugin;
})();
