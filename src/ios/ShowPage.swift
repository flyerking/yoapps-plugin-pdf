//
//  ShowPage.swift
//  app
//
//  Created by 陈鑫 on 2020/3/10.
//  Copyright © 2020 薛飞. All rights reserved.
//

import UIKit
import PDFKit
class ShowPage: UIView {
    init(pdfdocument:PDFDocument,pdfView:PDFView) {
        super.init(frame: CGRect(x: UIScreen.main.bounds.width - 100, y: UIScreen.main.bounds.height-80, width: 80, height: 30))
        self.pdfview = pdfView
        self.pdfdocument = pdfdocument
        pageNumber()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var pageView:UIButton!
    var pdfview:PDFView!
    var pdfdocument:PDFDocument!
    func pageNumber(){
        pageView = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
        pageView.backgroundColor = UIColor.gray
        pageView.setTitle("\(pdfview.currentPage?.label ?? "1")/\(pdfdocument.pageCount)", for: .normal)
        pageView.setTitleColor(.white, for: .normal)
        pageView.alpha = 0.5
        pageView.layer.cornerRadius = 15
        self.addSubview(pageView)
    }
}
