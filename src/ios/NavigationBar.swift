//
//  NavigationBar.swift
//  app
//
//  Created by 陈鑫 on 2020/3/9.
//  Copyright © 2020 薛飞. All rights reserved.
//

import UIKit
import PDFKit
protocol BarDelegate:class {
    func cancel()
    func goOtherApp()
}
extension  UIImage  {
    /**
     *  重设图片大小
     */
    func  reSizeImage(reSize: CGSize )-> UIImage  {
        //UIGraphicsBeginImageContext(reSize);
        UIGraphicsBeginImageContextWithOptions (reSize, false , UIScreen.main.scale);
        self.draw(in: CGRect(x: 0, y: 0, width: reSize.width, height: reSize.height))
        let reSizeImage: UIImage  =  UIGraphicsGetImageFromCurrentImageContext ()!;
        UIGraphicsEndImageContext ();
        return  reSizeImage;
    }
    
    /**
     *  等比率缩放
     */
    func  scaleImage(scaleSize: CGFloat )-> UIImage  {
        let  reSize =  CGSize ( width: self.size.width * scaleSize,  height: self.size.height * scaleSize)
        return  reSizeImage(reSize: reSize)
    }
}
class NavigationBar: UIView {
    var delegate:BarDelegate?
    var navigationItem:UINavigationItem!
    var pdfdocument:PDFDocument!
    var pdfView:PDFView!
    var leftLabel:UIBarButtonItem!
    var rightLabel:UIBarButtonItem!
    var navigationBar:UINavigationBar!
    var titleName:String!
    lazy var leftButton:UIButton = {
        let urlIcon = Bundle.main.url(forResource: "pdfcancel", withExtension: "png")
        let dataIcon = NSData(contentsOf: urlIcon!)
        let button:UIButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        let image = UIImage(data: dataIcon! as Data)
        button.setImage(image?.reSizeImage(reSize: CGSize(width: 20, height: 20)), for: .normal)
        //        /*向左进行偏移*/
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(cancel(_:)), for: .touchUpInside)
        return button
    }()
    lazy var rightButton:UIButton = {
        let urlIcon = Bundle.main.url(forResource: "pdfmore", withExtension: "png")
        let dataIcon = NSData(contentsOf: urlIcon!)
        let button:UIButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        let image = UIImage(data: dataIcon! as Data)
        button.setImage(image?.reSizeImage(reSize: CGSize(width: 20, height: 20)), for: .normal)
        //        /*向左进行偏移*/
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(goOtherApp(_:)), for: .touchUpInside)
        return button
    }()
    init(pdfducument:PDFDocument,pdfView:PDFView,titleName:String) {
        super.init(frame: CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 44))
        self.pdfdocument = pdfducument
        self.pdfView = pdfView
        self.titleName = titleName
        showNavigationBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func showNavigationBar(){
        self.backgroundColor = UIColor.white
        let screen = UIScreen.main.bounds
        let navigationBarHeight: CGFloat = 44//默认高度
        self.navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: screen.size.width, height: navigationBarHeight))
        leftLabel = UIBarButtonItem.init(customView: self.leftButton)
        navigationItem = UINavigationItem()
        navigationItem.leftBarButtonItem = leftLabel
        let titleView = UIView(frame: CGRect(x:0, y:0, width:200, height:40))
        let labelForTitle = UILabel(frame: CGRect(x:0, y:0, width:200, height:30))
        labelForTitle.font = UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.medium)
        labelForTitle.center = titleView.center
        labelForTitle.textAlignment = .center
        labelForTitle.text = titleName
        titleView.addSubview(labelForTitle)
        navigationItem.titleView = titleView
        rightLabel = UIBarButtonItem.init(customView: self.rightButton)
        navigationItem.rightBarButtonItem = rightLabel
        navigationBar.items = [navigationItem]
        navigationBar.barTintColor = UIColor.white  // 修改背景色
        self.addSubview(navigationBar)
    }
    @objc func cancel(_:Any){
        delegate?.cancel()
    }
    @objc func goOtherApp(_:Any){
        delegate?.goOtherApp()
    }
}
