package com.youth.yomapi.pdfView;

import android.content.Intent;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class PDFViewer extends CordovaPlugin {
    public static CallbackContext callbackContext;
    private String name = "name";
    private String pageNum = "pageNum";
    private String fileUrl = "fileUrl";
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        if("showPDF".equals(action)) {
            showPDF(args);
        }
        return true;
    }
    private void showPDF(JSONArray args) throws JSONException {
        Intent intent = new Intent(cordova.getActivity(), PDFViewActivity.class);
        intent.putExtra(name,args.getJSONObject(0).getString(name));
        intent.putExtra(pageNum,args.getJSONObject(0).getString(pageNum));
        intent.putExtra(fileUrl,args.getJSONObject(0).getString(fileUrl));
        cordova.getActivity().startActivity(intent);
    }
}
